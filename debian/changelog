python-flask-marshmallow (1.2.1-1) unstable; urgency=medium

  * Team upload.
  * [6123e1b] New upstream version 1.2.1
  * [d92beac] d/control: Use the GitHub URL in the Homepage field
  * [cd7c672] d/u/metadata: Use https for FAQ entry

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 22 Mar 2024 20:15:36 +0100

python-flask-marshmallow (1.2.0-1) unstable; urgency=medium

  * Team upload.
  * [f3358e7] New upstream version 1.2.0
  * [b2bc29a] Rebuild patch queue from patch-queue branch
    Removed patch:
    docs-Fix-Sphinx-build-with-7.2.patch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 07 Feb 2024 17:56:59 +0100

python-flask-marshmallow (1.1.0-1) unstable; urgency=medium

  * Team upload.
  * [aa94690] d/watch: Move over to git mode on GitHub
  * [1c8982a] New upstream version 1.1.0
  * [b1def61] Rebuild patch queue from patch-queue branch
    Added patch:
    tests-Ignore-tests-in-test_sqla.py-if-Flask-is-y-3.x.patch
  * [bea59a9] d/control: Adjust B-D due new upstream version
  * [023763e] d/rules: Adjust Sphinx based build
  * [2c98a6c] d/copyright: Update year data
  * [04a77b7] d/rules: Work around executable file permissions

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 21 Jan 2024 08:47:32 +0100

python-flask-marshmallow (0.15.0-3) unstable; urgency=medium

  * Team upload.
  * Rebuild patch queue from patch-queue branch
    Added patch:
    docs-Fix-Sphinx-build-with-7.2.patch
    (Closes: #1042586)
  * d/rules: Don't compress changelog.html

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 03 Dec 2023 10:59:26 +0100

python-flask-marshmallow (0.15.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 23 Jun 2023 15:58:51 +0200

python-flask-marshmallow (0.15.0-1) experimental; urgency=medium

  * Team upload.
  [ Debian Janitor ]
  * Update standards version to 4.6.2, no changes needed.

  [ Carsten Schoenert ]
  * New upstream version 0.15.0
  * Rebuild patch queue from patch-queue branch
    Drop the existing patches, they are not needed any more. Adding new
    patch regarding the sphinxdoc based documentation.
    Removed patches:
    test-Use-pytest.fixture-instead-of-pytest.yield_fixture.patch
    tests-Import-Response-if-werkzeug-2.1.0.patch
    Added patches:
    docs-Don-t-use-external-intersphinx.patch
  * d/*: Running wrap-and-sort -ast
  * d/copyright: Update year data and copyright holder
  * d/control: Adjust B-D entries, add BuildProfileSpecs
  * d/rules: Drop --with option in default target
  * d/u/m/: Add FAQ entry
  * python-flask-marshmallow-doc: Add new binary package
  * d/python-flask-marshmallow-doc: Add doc-base sequencer
  * d/README.source: Add hint about missing -doc package

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 09 Apr 2023 09:18:40 +0200

python-flask-marshmallow (0.14.0-3) unstable; urgency=medium

  * Team upload.
  * gbp: Add a starting config
  * Add patches from patch-queue branch
    Added patches:
    test-Use-pytest.fixture-instead-of-pytest.yield_fixture.patch
    tests-Import-Response-if-werkzeug-2.1.0.patch
  * autopkgtest: Restructure and extend output of test call
  * d/README.source: Add a basic information
  * Rename docs -> python3-flask-marshmallow.docs
  * d/control: Update Standards-Version to 4.6.1
    No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 13 Oct 2022 20:10:28 +0200

python-flask-marshmallow (0.14.0-2) unstable; urgency=medium

  * Team upload.
  * No-change rebuild for unstable. (Closes: #1002385)

 -- Gabriela Pivetta <gpivetta99@gmail.com>  Sun, 14 Aug 2022 06:19:21 -0300

python-flask-marshmallow (0.14.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Emmanuel Arias ]
  * New upstream version 0.14.0 (Closes: #989269)
  * d/control: Bump Standards-Versions to 4.5.1
  * d/control: bump debhelper-compat to 13

  [ Debian Janitor ]
  * Remove obsolete field Name from d/u//metadata.

  [ Utkarsh Gupta ]
  * Use my @d.o email address

 -- Utkarsh Gupta <utkarsh@debian.org>  Fri, 21 Jan 2022 19:23:28 +0530

python-flask-marshmallow (0.10.1-4) unstable; urgency=medium

  * Add python3-importlib-metadata as a dependency
    (as per buildd logs)

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 29 Oct 2019 12:12:53 +0530

python-flask-marshmallow (0.10.1-3) unstable; urgency=medium

  * Update d/copyright (Closes: #942456)
  * Close RFP bug (Closes: #819645)

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 22 Oct 2019 19:15:06 +0530

python-flask-marshmallow (0.10.1-2) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.4.1
  * Run wrap-and-sort

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Thu, 17 Oct 2019 22:36:26 +0530

python-flask-marshmallow (0.10.1-1) experimental; urgency=medium

  * Initial release (Closes: #934348)

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Fri, 09 Aug 2019 21:16:53 +0530
